﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SistemaLabMQ.Models;

namespace SistemaLabMQ.Reportes
{
    public partial class frmInforme : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int IdMuestra = Convert.ToInt32(Request.QueryString["IdMuestra"].ToString());

            LabmqDB db = new LabmqDB();
            paramInformeResultados param = db.paramInformeResultados.Where(s =>s.IdMuestra==IdMuestra).FirstOrDefault();

            //int IdEntrada = Convert.ToInt32(Request.QueryString["IdEntrada"].ToString());
            //int ValorM = Convert.ToInt32(Request.QueryString["ValorM"].ToString());
            //int ValorR = Convert.ToInt32(Request.QueryString["ValorR"].ToString());
            //int Imagen = Convert.ToInt32(Request.QueryString["Imagen"].ToString());
            //int EnsayoI = Convert.ToInt32(Request.QueryString["EnsayoI"].ToString());
            //int LME = Convert.ToInt32(Request.QueryString["LME"].ToString());
            //int LME1 = Convert.ToInt32(Request.QueryString["LME1"].ToString());
            //int LCalculoI = Convert.ToInt32(Request.QueryString["LCalculoI"].ToString());
            //int EnsayoA = Convert.ToInt32(Request.QueryString["EnsayoA"].ToString());
            //int LSilice = Convert.ToInt32(Request.QueryString["LSilice"].ToString());
            //int LValoresM = Convert.ToInt32(Request.QueryString["LValoresM"].ToString());
            //string LeyendaP = Request.QueryString["LeyendaP"].ToString();
            ////int Firmas= Convert.ToInt32(Request.QueryString["Firmas"].ToString());
            //int TipoI= Convert.ToInt32(Request.QueryString["TipoI"].ToString());
            //int Salto= Convert.ToInt32(Request.QueryString["Salto"].ToString());
            //int LValoresR= Convert.ToInt32(Request.QueryString["LValoresR"].ToString());

            if (!IsPostBack)
            {
                //if (param.Salto == 1)
                //{
                //    ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/rptInformeSalto.rdlc";
                //}
                //else
                //{
                //    ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/rptInforme.rdlc";
                //}

                switch (param.TipoI)
                {
                    case 1:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaPotable/rptInformeAguaPotable1.rdlc";
                        break;
                    case 2:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaPotable/rptInformeAguaPotable2.rdlc";
                        break;
                    case 3:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaPotable/rptInformeAguaPotable3.rdlc";
                        break;
                    case 4:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaResidual/rptInformeAguaResidual1.rdlc";
                        break;
                    case 5:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaResidual/rptInformeAguaResidual2.rdlc";
                        break;
                    case 6:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaResidual/rptInformeAguaResidual3.rdlc";
                        break;
                    case 7:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Alimentos/rptInformeAlimentos1.rdlc";
                        break;
                    case 8:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Alimentos/rptInformeAlimentos2.rdlc";
                        break;
                    case 9:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Alimentos/rptInformeAlimentos3.rdlc";
                        break;
                    case 10:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Hisopos/rptInformeHisopos1.rdlc";
                        break;
                    case 11:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Hisopos/rptInformeHisopos2.rdlc";
                        break;
                    case 12:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/MateriaPrima/rptInformeMateriaPrima1.rdlc";
                        ReportParameter rParamCumple = new ReportParameter("Cumple", param.Cumple.ToString());
                        ReportViewer2.LocalReport.SetParameters(rParamCumple);
                        ReportParameter rParamNoCumple = new ReportParameter("NoCumple", param.Cumple==1?"0":"1");
                        ReportViewer2.LocalReport.SetParameters(rParamNoCumple);
                        break;
                    case 19:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/MateriaPrima/rptInformeMateriaPrimaProd1.rdlc";
                        ReportParameter rParamCumple4 = new ReportParameter("Cumple", param.Cumple.ToString());
                        ReportViewer2.LocalReport.SetParameters(rParamCumple4);
                        ReportParameter rParamNoCumple3 = new ReportParameter("NoCumple", param.Cumple == 1 ? "0" : "1");
                        ReportViewer2.LocalReport.SetParameters(rParamNoCumple3);
                        break;
                    case 13:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/MateriaPrima/rptInformeMateriaPrima2.rdlc";
                        ReportParameter rParamCumple2 = new ReportParameter("Cumple", param.Cumple.ToString());
                        ReportViewer2.LocalReport.SetParameters(rParamCumple2);
                        ReportParameter rParamNoCumple2 = new ReportParameter("NoCumple", param.Cumple == 1 ? "0" : "1");
                        ReportViewer2.LocalReport.SetParameters(rParamNoCumple2);
                        break;
                    case 14:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/PoderBactericida/rptInformePoderBactericida.rdlc";
                        break;
                    case 15:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/VidaUtil/rptInformeVidaUtil.rdlc";
                        ReportParameter rAmbiente = new ReportParameter("Temperatura", param.Temperatura.ToString());
                        ReportViewer2.LocalReport.SetParameters(rAmbiente);
                        break;
                    case 16:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Productos/rptInformeProductos1.rdlc";
                        break;
                    case 17:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Productos/rptInformeProductos2.rdlc";
                        break;
                    case 18:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Suelo/rptInformeSuelo.rdlc";
                        break;
                    default:
                        break;
                }
                ReportViewer2.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer2.LocalReport.DataSources.Clear();
                DataTable dt = rptInforme(IdMuestra);
                ReportDataSource rds = new ReportDataSource("DsInformeTecnico", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds);

                dt = rptEnsayoNoAcreditado(IdMuestra);
                ReportDataSource rds2 = new ReportDataSource("DsEnsayoNoAcreditado", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds2);

                dt = rptParametros();
                ReportDataSource rds3 = new ReportDataSource("DsParametros1", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds3);

                dt = DsAnalisisSoli(IdMuestra);
                if (param.EnsayoA == 2)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string analisis = row["Analisis"].ToString();

                        string cleanAnalisis = analisis.Replace("**", "");

                        row["Analisis"] = cleanAnalisis;
                    }
                }
                ReportDataSource rds4 = new ReportDataSource("DsAnalisisSoli", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds4);

                dt = DsFirmasMuestra(IdMuestra);
                DataTable dt2 = dt.Copy();

                if (dt.Rows.Count <= 1)
                {
                    if (dt.Rows.Count == 1)
                    {
                        dt2.Rows.Remove(dt2.Rows[0]);
                    }
                    dt2.Rows.Add(1, "Firma 1", "");
                }
                else
                {
                    dt.Rows.Remove(dt.Rows[1]);
                    dt2.Rows.Remove(dt2.Rows[0]);
                }

                ReportDataSource rds5 = new ReportDataSource("DsFirmasMuestra", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds5);

                ReportDataSource rds6 = new ReportDataSource("DsFirmasMuestra1", dt2);
                ReportViewer2.LocalReport.DataSources.Add(rds6);

                //ReportParameter rParamValorM = new ReportParameter("ValorM", param.ValorM.ToString());
                //ReportParameter rParamValorR = new ReportParameter("ValorR", param.ValorR.ToString());
                //ReportParameter rParamImagen = new ReportParameter("Imagen", param.Imagen.ToString());
                //ReportParameter rParamEnsayoI = new ReportParameter("EnsayoI", param.EnsayoI.ToString());
                //ReportParameter rParamLME = new ReportParameter("LME", param.LME.ToString());
                //ReportParameter rParamLME1 = new ReportParameter("LME1", param.LME1.ToString());
                //ReportParameter rParamLCalculoI = new ReportParameter("LCalculoI", param.LCalculoI.ToString());
                //ReportParameter rParamEnsayoA = new ReportParameter("EnsayoA", param.EnsayoA.ToString());
                //ReportParameter rParamLSilice = new ReportParameter("LSilice", param.LSilice.ToString());
                //ReportParameter rParamLValoresM = new ReportParameter("LValoresM", param.LValoresM.ToString());
                ReportParameter rLeyendaP = new ReportParameter("LeyendaP", param.LeyendaP.ToString());
                //ReportParameter rFirmas = new ReportParameter("Firmas", Firmas.ToString());
                //ReportParameter rTipoI = new ReportParameter("TipoI", param.TipoI.ToString());
                //ReportParameter rLValoresR = new ReportParameter("LValoresR", param.LValoresR.ToString());

                //ReportViewer2.LocalReport.SetParameters(rParamValorM);
                //ReportViewer2.LocalReport.SetParameters(rParamValorR);
                //ReportViewer2.LocalReport.SetParameters(rParamImagen);
                //ReportViewer2.LocalReport.SetParameters(rParamEnsayoI);
                //ReportViewer2.LocalReport.SetParameters(rParamLME);
                //ReportViewer2.LocalReport.SetParameters(rParamLME1);
                //ReportViewer2.LocalReport.SetParameters(rParamLCalculoI);
                //ReportViewer2.LocalReport.SetParameters(rParamEnsayoA);
                //ReportViewer2.LocalReport.SetParameters(rParamLSilice);
                //ReportViewer2.LocalReport.SetParameters(rParamLValoresM);
                ReportViewer2.LocalReport.SetParameters(rLeyendaP);
                //ReportViewer2.LocalReport.SetParameters(rFirmas);
                //ReportViewer2.LocalReport.SetParameters(rTipoI);
                //ReportViewer2.LocalReport.SetParameters(rLValoresR);

                ReportViewer2.LocalReport.DisplayName = "Muestra_" + IdMuestra;
                ReportViewer2.LocalReport.Refresh();
                ReportViewer2.DataBind();
            }
        }

        private DataTable rptParametros()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_getParametros", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
        private DataTable rptInforme(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptInforme", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);

            conn.Close();
            conn.Dispose();
            return dt;
        }
        private DataTable rptEnsayoNoAcreditado(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptEnsayoNoAcreditado", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdCorrelativoMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
        private DataTable DsAnalisisSoli(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptAnalisisSolicitados", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
        private DataTable DsFirmasMuestra(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_getFirmasMuestra", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
    }
}