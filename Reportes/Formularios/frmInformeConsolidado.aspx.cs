﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Reporting.WebForms;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using SistemaLabMQ.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Microsoft.Office.Interop.Word;

namespace SistemaLabMQ.Reportes.Formularios
{
    public partial class frmInformeConsolidado : System.Web.UI.Page
    {
        LabmqDB db = new LabmqDB();
        protected void Page_Load(object sender, EventArgs e)
        {
            string IdMuestraString = Request.QueryString["IdMuestra"].ToString();
            string Formato = Request.QueryString["Formato"].ToString();

            List<byte[]> docs = new List<byte[]>();

            List<int> idMuestraList = IdMuestraString.Split(',')
                                                 .Select(id => int.Parse(id.Trim()))
                                                 .ToList();
            //string[] idAnalisisArray = IdMuestraString.Split(',');

            //int[] idAnalisisInt = Array.ConvertAll(idAnalisisArray, int.Parse);

            //int IdMuestraInicial = idAnalisisInt[0];
            List<paramInformeResultados> parametros = db.paramInformeResultados.Where(s => idMuestraList.Contains(s.IdMuestra)).ToList();

            //DataTable dtFirmas= DsFirmasMuestra(IdMuestraInicial);

            if (Formato.ToUpper() == "PDF")
            {
                foreach (paramInformeResultados param in parametros)
                {
                   byte[] pdf = GeneraInforme(param.IdMuestra, param, 1);
                   docs.Add(pdf);

                }

                byte[] pdfFinal = CombinarPDFs(docs);

                //Response.Clear();
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("content-disposition", "attachment;filename=InformeConsolidado_CRR"+parametros[0].IdEntrada+".pdf");
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.BinaryWrite(pdfFinal);
                //Response.End();

                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "inline;filename=InformeConsolidado_CRR" + parametros[0].IdEntrada + ".pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(pdfFinal);
                Response.End();
            }
            else
            {
                foreach (paramInformeResultados param in parametros)
                {
                    byte[] word = GeneraInforme(param.IdMuestra, param, 2);
                    docs.Add(word);

                    //Response.Clear();
                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; // Tipo MIME para Word
                    //Response.AddHeader("Content-Disposition", "attachment;filename=InformeConsolidado_CRR" + parametros[0].IdEntrada + ".docx"); // Nombre del archivo
                    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //Response.BinaryWrite(word); // Enviar el contenido del archivo como bytes
                    //Response.End();
                }

                byte[] wordFinal = CombinarWord(docs);

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; // Tipo MIME para Word
                Response.AddHeader("Content-Disposition", "attachment;filename=InformeConsolidado_CRR" + parametros[0].IdEntrada + ".docx"); // Nombre del archivo
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(wordFinal); // Enviar el contenido del archivo como bytes
                Response.End();

            }


        }

        public byte[] CombinarWord(List<byte[]> documentos)
        {
            using (MemoryStream documentoUnificado = new MemoryStream())
            {
                // Crear un documento Word vacío para empezar
                using (WordprocessingDocument docUnificado = WordprocessingDocument.Create(
                    documentoUnificado,
                    DocumentFormat.OpenXml.WordprocessingDocumentType.Document))
                {
                    // Crear el cuerpo inicial del documento
                    docUnificado.AddMainDocumentPart();
                    docUnificado.MainDocumentPart.Document = new Document(new Body());

                    Body cuerpoUnificado = docUnificado.MainDocumentPart.Document.Body;

                    foreach (var bytes in documentos)
                    {
                        using (MemoryStream stream = new MemoryStream(bytes))
                        {
                            using (WordprocessingDocument documentoIndividual = WordprocessingDocument.Open(stream, false))
                            {
                                // Copiar el contenido del documento actual
                                Body cuerpoActual = documentoIndividual.MainDocumentPart.Document.Body;
                                foreach (var elemento in cuerpoActual.Elements())
                                {
                                    // Clonar los elementos para evitar conflictos
                                    cuerpoUnificado.Append(elemento.CloneNode(true));
                                }
                            }
                        }

                        // Agregar un salto de página entre documentos
                        cuerpoUnificado.Append(new Paragraph(new Run(new Break() { Type = BreakValues.Page })));
                    }
                }

                return documentoUnificado.ToArray(); // Devolver el documento combinado como un arreglo de bytes
            }
        }
        public byte[] CombinarPDFs(List<byte[]> pdfs)
        {
            using (MemoryStream outputStream = new MemoryStream())
            {
                PdfDocument outputDocument = new PdfDocument();

                foreach (var pdf in pdfs)
                {
                    using (MemoryStream inputPdfStream = new MemoryStream(pdf))
                    {
                        PdfDocument inputDocument = PdfReader.Open(inputPdfStream, PdfDocumentOpenMode.Import);

                        for (int i = 0; i < inputDocument.PageCount; i++)
                        {
                            PdfPage page = inputDocument.Pages[i];
                            outputDocument.AddPage(page);
                        }
                    }
                }

                outputDocument.Save(outputStream);
                return outputStream.ToArray();
            }
        }
        public byte[] GenerarInforme(int IdMuestra, paramInformeResultados param, DataTable firmas, int Tipo)
        {
            if (!IsPostBack)
            {
                if (param.Salto == 1)
                {
                    ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/rptInformeSalto.rdlc";
                }
                else
                {
                    ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/rptInforme.rdlc";
                }

                ReportViewer2.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer2.LocalReport.DataSources.Clear();
                DataTable dt = rptInforme(IdMuestra);
                ReportDataSource rds = new ReportDataSource("DsInformeTecnico", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds);

                dt = rptEnsayoNoAcreditado(IdMuestra);
                ReportDataSource rds2 = new ReportDataSource("DsEnsayoNoAcreditado", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds2);

                dt = rptParametros();
                ReportDataSource rds3 = new ReportDataSource("DsParametros1", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds3);

                dt = DsAnalisisSoli(IdMuestra);
                if (param.EnsayoA == 2)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string analisis = row["Analisis"].ToString();

                        string cleanAnalisis = analisis.Replace("**", "");

                        row["Analisis"] = cleanAnalisis;
                    }
                }
                ReportDataSource rds4 = new ReportDataSource("DsAnalisisSoli", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds4);

                dt = firmas.Copy();
                DataTable dt2 = dt.Copy();

                if (dt.Rows.Count <= 1)
                {
                    if (dt.Rows.Count == 1)
                    {
                        dt2.Rows.Remove(dt2.Rows[0]);
                    }
                    dt2.Rows.Add(1, "Firma 1", "");
                }
                else
                {
                    dt.Rows.Remove(dt.Rows[1]);
                    dt2.Rows.Remove(dt2.Rows[0]);
                }

                ReportDataSource rds5 = new ReportDataSource("DsFirmasMuestra", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds5);

                ReportDataSource rds6 = new ReportDataSource("DsFirmasMuestra1", dt2);
                ReportViewer2.LocalReport.DataSources.Add(rds6);

                ReportParameter rParamValorM = new ReportParameter("ValorM", param.ValorM.ToString());
                ReportParameter rParamValorR = new ReportParameter("ValorR", param.ValorR.ToString());
                ReportParameter rParamImagen = new ReportParameter("Imagen", param.Imagen.ToString());
                ReportParameter rParamEnsayoI = new ReportParameter("EnsayoI", param.EnsayoI.ToString());
                ReportParameter rParamLME = new ReportParameter("LME", param.LME.ToString());
                ReportParameter rParamLME1 = new ReportParameter("LME1", param.LME1.ToString());
                ReportParameter rParamLCalculoI = new ReportParameter("LCalculoI", param.LCalculoI.ToString());
                ReportParameter rParamEnsayoA = new ReportParameter("EnsayoA", param.EnsayoA.ToString());
                ReportParameter rParamLSilice = new ReportParameter("LSilice", param.LSilice.ToString());
                ReportParameter rParamLValoresM = new ReportParameter("LValoresM", param.LValoresM.ToString());
                ReportParameter rLeyendaP = new ReportParameter("LeyendaP", param.LeyendaP.ToString());
                //ReportParameter rFirmas = new ReportParameter("Firmas", Firmas.ToString());
                ReportParameter rTipoI = new ReportParameter("TipoI", param.TipoI.ToString());
                ReportParameter rLValoresR = new ReportParameter("LValoresR", param.LValoresR.ToString());

                ReportViewer2.LocalReport.SetParameters(rParamValorM);
                ReportViewer2.LocalReport.SetParameters(rParamValorR);
                ReportViewer2.LocalReport.SetParameters(rParamImagen);
                ReportViewer2.LocalReport.SetParameters(rParamEnsayoI);
                ReportViewer2.LocalReport.SetParameters(rParamLME);
                ReportViewer2.LocalReport.SetParameters(rParamLME1);
                ReportViewer2.LocalReport.SetParameters(rParamLCalculoI);
                ReportViewer2.LocalReport.SetParameters(rParamEnsayoA);
                ReportViewer2.LocalReport.SetParameters(rParamLSilice);
                ReportViewer2.LocalReport.SetParameters(rParamLValoresM);
                ReportViewer2.LocalReport.SetParameters(rLeyendaP);
                //ReportViewer2.LocalReport.SetParameters(rFirmas);
                ReportViewer2.LocalReport.SetParameters(rTipoI);
                ReportViewer2.LocalReport.SetParameters(rLValoresR);

                ReportViewer2.LocalReport.DisplayName = "Muestra_" + IdMuestra;
                ReportViewer2.LocalReport.Refresh();
                ReportViewer2.DataBind();
            }

            // Exportar el informe a un byte array en formato PDF
            string mimeType;
            string encoding;
            string fileNameExtension;
            string[] streams;
            Warning[] warnings;

            
            byte[] bytes = ReportViewer2.LocalReport.Render(
               Tipo==1? "PDF": "WORDOPENXML", null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            string a = fileNameExtension;

            return bytes; // Devuelve el PDF en formato byte array
        }

        public byte[] GeneraInforme(int IdMuestra, paramInformeResultados param, int Tipo)
        {
            if (!IsPostBack)
            {
                //if (param.Salto == 1)
                //{
                //    ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/rptInformeSalto.rdlc";
                //}
                //else
                //{
                //    ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/rptInforme.rdlc";
                //}

                switch (param.TipoI)
                {
                    case 1:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaPotable/rptInformeAguaPotable1.rdlc";
                        break;
                    case 2:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaPotable/rptInformeAguaPotable2.rdlc";
                        break;
                    case 3:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaPotable/rptInformeAguaPotable3.rdlc";
                        break;
                    case 4:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaResidual/rptInformeAguaResidual1.rdlc";
                        break;
                    case 5:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaResidual/rptInformeAguaResidual2.rdlc";
                        break;
                    case 6:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/AguaResidual/rptInformeAguaResidual3.rdlc";
                        break;
                    case 7:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Alimentos/rptInformeAlimentos1.rdlc";
                        break;
                    case 8:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Alimentos/rptInformeAlimentos2.rdlc";
                        break;
                    case 9:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Alimentos/rptInformeAlimentos3.rdlc";
                        break;
                    case 10:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Hisopos/rptInformeHisopos1.rdlc";
                        break;
                    case 11:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Hisopos/rptInformeHisopos2.rdlc";
                        break;
                    case 12:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/MateriaPrima/rptInformeMateriaPrima1.rdlc";
                        ReportParameter rParamCumple = new ReportParameter("Cumple", param.Cumple.ToString());
                        ReportViewer2.LocalReport.SetParameters(rParamCumple);
                        ReportParameter rParamNoCumple = new ReportParameter("NoCumple", param.Cumple == 1 ? "0" : "1");
                        ReportViewer2.LocalReport.SetParameters(rParamNoCumple);
                        break;
                    case 13:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/MateriaPrima/rptInformeMateriaPrima2.rdlc";
                        ReportParameter rParamCumple2 = new ReportParameter("Cumple", param.Cumple.ToString());
                        ReportViewer2.LocalReport.SetParameters(rParamCumple2);
                        ReportParameter rParamNoCumple2 = new ReportParameter("NoCumple", param.Cumple == 1 ? "0" : "1");
                        ReportViewer2.LocalReport.SetParameters(rParamNoCumple2);
                        break;
                    case 14:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/PoderBactericida/rptInformePoderBactericida.rdlc";
                        break;
                    case 15:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/VidaUtil/rptInformeVidaUtil.rdlc";
                        ReportParameter rAmbiente = new ReportParameter("Temperatura", param.Temperatura.ToString());
                        ReportViewer2.LocalReport.SetParameters(rAmbiente);
                        break;
                    case 16:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Productos/rptInformeProductos1.rdlc";
                        break;
                    case 17:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Productos/rptInformeProductos2.rdlc";
                        break;
                    case 18:
                        ReportViewer2.LocalReport.ReportPath = "Reportes/Rpt/Resultados/Suelo/rptInformeSuelo.rdlc";
                        break;
                    default:
                        break;
                }
                ReportViewer2.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer2.LocalReport.DataSources.Clear();
                DataTable dt = rptInforme(IdMuestra);
                ReportDataSource rds = new ReportDataSource("DsInformeTecnico", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds);

                dt = rptEnsayoNoAcreditado(IdMuestra);
                ReportDataSource rds2 = new ReportDataSource("DsEnsayoNoAcreditado", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds2);

                dt = rptParametros();
                ReportDataSource rds3 = new ReportDataSource("DsParametros1", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds3);

                dt = DsAnalisisSoli(IdMuestra);
                if (param.EnsayoA == 2)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string analisis = row["Analisis"].ToString();

                        string cleanAnalisis = analisis.Replace("**", "");

                        row["Analisis"] = cleanAnalisis;
                    }
                }
                ReportDataSource rds4 = new ReportDataSource("DsAnalisisSoli", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds4);

                dt = DsFirmasMuestra(IdMuestra);
                DataTable dt2 = dt.Copy();

                if (dt.Rows.Count <= 1)
                {
                    if (dt.Rows.Count == 1)
                    {
                        dt2.Rows.Remove(dt2.Rows[0]);
                    }
                    dt2.Rows.Add(1, "Firma 1", "");
                }
                else
                {
                    dt.Rows.Remove(dt.Rows[1]);
                    dt2.Rows.Remove(dt2.Rows[0]);
                }

                ReportDataSource rds5 = new ReportDataSource("DsFirmasMuestra", dt);
                ReportViewer2.LocalReport.DataSources.Add(rds5);

                ReportDataSource rds6 = new ReportDataSource("DsFirmasMuestra1", dt2);
                ReportViewer2.LocalReport.DataSources.Add(rds6);

                ReportParameter rLeyendaP = new ReportParameter("LeyendaP", param.LeyendaP.ToString());
                ReportViewer2.LocalReport.SetParameters(rLeyendaP);

                ReportViewer2.LocalReport.DisplayName = "Muestra_" + IdMuestra;
                ReportViewer2.LocalReport.Refresh();
                ReportViewer2.DataBind();
            }

            string mimeType;
            string encoding;
            string fileNameExtension;
            string[] streams;
            Warning[] warnings;

            byte[] bytes = ReportViewer2.LocalReport.Render(
                Tipo == 1 ? "PDF" : "WORDOPENXML", null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            return bytes;
        }

        /*public byte[] ConvertirDocADocx(byte[] bytes)
        {
            Application wordApp = new Application();
            Document doc = null;
            string tempDocPath = Path.GetTempFileName() + ".doc";
            string tempDocxPath = Path.ChangeExtension(tempDocPath, ".docx");

            try
            {
                // Guardar los bytes como un archivo temporal .doc
                File.WriteAllBytes(tempDocPath, bytes);

                // Abrir el archivo temporal
                doc = wordApp.Documents.Open(tempDocPath);

                // Guardar como .docx
                doc.SaveAs2(tempDocxPath, WdSaveFormat.wdFormatXMLDocument);

                // Leer el archivo .docx como bytes
                return File.ReadAllBytes(tempDocxPath);
            }
            finally
            {
                // Limpiar recursos
                doc?.Close();
                wordApp.Quit();
                File.Delete(tempDocPath);
                File.Delete(tempDocxPath);
            }
        }*/
        private DataTable rptParametros()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_getParametros", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
        private DataTable rptInforme(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptInforme", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);

            conn.Close();
            conn.Dispose();
            return dt;
        }
        private DataTable rptEnsayoNoAcreditado(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptEnsayoNoAcreditado", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdCorrelativoMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
        private DataTable DsAnalisisSoli(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptAnalisisSolicitados", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
        private DataTable DsFirmasMuestra(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_getFirmasMuestra", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
    }
}