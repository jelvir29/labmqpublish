﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SistemaLabMQ.Reportes.Formularios
{
    public partial class frmInformeGenerico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int IdMuestra = Convert.ToInt32(Request.QueryString["IdMuestra"].ToString());
            int IdEntrada = Convert.ToInt32(Request.QueryString["IdEntrada"].ToString());
            int ValorM = Convert.ToInt32(Request.QueryString["ValorM"].ToString());
            int ValorR = Convert.ToInt32(Request.QueryString["ValorR"].ToString());
            int Imagen = Convert.ToInt32(Request.QueryString["Imagen"].ToString());
            int EnsayoI = Convert.ToInt32(Request.QueryString["EnsayoI"].ToString());
            int LME = Convert.ToInt32(Request.QueryString["LME"].ToString());
            int LME1 = Convert.ToInt32(Request.QueryString["LME1"].ToString());
            int LCalculoI = Convert.ToInt32(Request.QueryString["LCalculoI"].ToString());
            int EnsayoA = Convert.ToInt32(Request.QueryString["EnsayoA"].ToString());
            int LSilice = Convert.ToInt32(Request.QueryString["LSilice"].ToString());
            int LValoresM = Convert.ToInt32(Request.QueryString["LValoresM"].ToString());
            string LeyendaP = Request.QueryString["LeyendaP"].ToString();
            //int Firmas= Convert.ToInt32(Request.QueryString["Firmas"].ToString());
            int TipoI = Convert.ToInt32(Request.QueryString["TipoI"].ToString());
            int Salto = Convert.ToInt32(Request.QueryString["Salto"].ToString());
            int LValoresR = Convert.ToInt32(Request.QueryString["LValoresR"].ToString());

            if (!IsPostBack)
            {
                if (Salto == 1)
                {
                    ReportViewer1.LocalReport.ReportPath = "Reportes/Rpt/rptInformeSalto.rdlc";
                }
                else
                {
                    ReportViewer1.LocalReport.ReportPath = "Reportes/Rpt/rptInforme.rdlc";
                }

                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.DataSources.Clear();
                DataTable dt = rptInforme(IdMuestra);
                ReportDataSource rds = new ReportDataSource("DsInformeTecnico", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds);

                dt = rptEnsayoNoAcreditado(IdMuestra);
                ReportDataSource rds2 = new ReportDataSource("DsEnsayoNoAcreditado", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds2);

                dt = rptParametros();
                ReportDataSource rds3 = new ReportDataSource("DsParametros1", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds3);

                dt = DsAnalisisSoli(IdMuestra);
                if (EnsayoA == 2)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string analisis = row["Analisis"].ToString();

                        string cleanAnalisis = analisis.Replace("**", "");

                        row["Analisis"] = cleanAnalisis;
                    }
                }
                ReportDataSource rds4 = new ReportDataSource("DsAnalisisSoli", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds4);

                dt = DsFirmasMuestra(IdMuestra);
                DataTable dt2 = dt.Copy();

                if (dt.Rows.Count <= 1)
                {
                    if (dt.Rows.Count == 1)
                    {
                        dt2.Rows.Remove(dt2.Rows[0]);
                    }
                    dt2.Rows.Add(1, "Firma 1", "");
                }
                else
                {
                    dt.Rows.Remove(dt.Rows[1]);
                    dt2.Rows.Remove(dt2.Rows[0]);
                }

                ReportDataSource rds5 = new ReportDataSource("DsFirmasMuestra", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds5);

                ReportDataSource rds6 = new ReportDataSource("DsFirmasMuestra1", dt2);
                ReportViewer1.LocalReport.DataSources.Add(rds6);

                ReportParameter rParamValorM = new ReportParameter("ValorM", ValorM.ToString());
                ReportParameter rParamValorR = new ReportParameter("ValorR", ValorR.ToString());
                ReportParameter rParamImagen = new ReportParameter("Imagen", Imagen.ToString());
                ReportParameter rParamEnsayoI = new ReportParameter("EnsayoI", EnsayoI.ToString());
                ReportParameter rParamLME = new ReportParameter("LME", LME.ToString());
                ReportParameter rParamLME1 = new ReportParameter("LME1", LME1.ToString());
                ReportParameter rParamLCalculoI = new ReportParameter("LCalculoI", LCalculoI.ToString());
                ReportParameter rParamEnsayoA = new ReportParameter("EnsayoA", EnsayoA.ToString());
                ReportParameter rParamLSilice = new ReportParameter("LSilice", LSilice.ToString());
                ReportParameter rParamLValoresM = new ReportParameter("LValoresM", LValoresM.ToString());
                ReportParameter rLeyendaP = new ReportParameter("LeyendaP", LeyendaP.ToString());
                //ReportParameter rFirmas = new ReportParameter("Firmas", Firmas.ToString());
                ReportParameter rTipoI = new ReportParameter("TipoI", TipoI.ToString());
                ReportParameter rLValoresR = new ReportParameter("LValoresR", LValoresR.ToString());

                ReportViewer1.LocalReport.SetParameters(rParamValorM);
                ReportViewer1.LocalReport.SetParameters(rParamValorR);
                ReportViewer1.LocalReport.SetParameters(rParamImagen);
                ReportViewer1.LocalReport.SetParameters(rParamEnsayoI);
                ReportViewer1.LocalReport.SetParameters(rParamLME);
                ReportViewer1.LocalReport.SetParameters(rParamLME1);
                ReportViewer1.LocalReport.SetParameters(rParamLCalculoI);
                ReportViewer1.LocalReport.SetParameters(rParamEnsayoA);
                ReportViewer1.LocalReport.SetParameters(rParamLSilice);
                ReportViewer1.LocalReport.SetParameters(rParamLValoresM);
                ReportViewer1.LocalReport.SetParameters(rLeyendaP);
                //ReportViewer1.LocalReport.SetParameters(rFirmas);
                ReportViewer1.LocalReport.SetParameters(rTipoI);
                ReportViewer1.LocalReport.SetParameters(rLValoresR);

                ReportViewer1.LocalReport.DisplayName = "Muestra_" + IdMuestra;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.DataBind();
            }

        }
        private DataTable rptParametros()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_getParametros", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }
        private DataTable rptInforme(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptInforme", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);

            conn.Close();
            conn.Dispose();
            return dt;
        }

        private DataTable rptEnsayoNoAcreditado(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptEnsayoNoAcreditado", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdCorrelativoMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }

        private DataTable DsAnalisisSoli(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptAnalisisSolicitados", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }

        private DataTable DsFirmasMuestra(int IdMuestra)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_getFirmasMuestra", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@IdMuestra", IdMuestra);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);
            conn.Close();
            conn.Dispose();

            return dt;
        }

    }
}