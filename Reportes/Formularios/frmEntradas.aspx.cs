﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SistemaLabMQ.Reportes
{
    public partial class frmEntradas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int idEntrada = Convert.ToInt32(Request.QueryString["idEntrada"].ToString());

            if (!IsPostBack)
            {
                ReportViewer1.LocalReport.ReportPath = "Reportes/Rpt/rptEntrada.rdlc";
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.DataSources.Clear();
                DataTable dt = rptEntrada(idEntrada);
                ReportDataSource rds = new ReportDataSource("DsEntrada", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds);

                dt = rptMuestras(idEntrada);
                ReportDataSource rds2 = new ReportDataSource("DsMuestras", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds2);

                dt = rptParametros();
                ReportDataSource rds3 = new ReportDataSource("DsParametros", dt);
                ReportViewer1.LocalReport.DataSources.Add(rds3);

                ReportViewer1.LocalReport.DisplayName = "Entrada"+idEntrada;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.DataBind();
            }
        }

        private DataTable rptParametros()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_getParametros", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);


            return dt;
        }
        private DataTable rptEntrada(int idEntrada)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptEntrada", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@idEntrada", idEntrada);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);


            return dt;
        }

        private DataTable rptMuestras(int idEntrada)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);
            SqlCommand cmm = new SqlCommand("sp_rptMuestras", conn);
            cmm.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParameter = new SqlParameter("@idEntrada", idEntrada);
            cmm.Parameters.Add(sqlParameter);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = cmm;
            dataAdapter.Fill(dt);


            return dt;
        }
    }
}