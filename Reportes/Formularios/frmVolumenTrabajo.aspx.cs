﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SistemaLabMQ.Reportes.Formularios
{
    public partial class frmVolumenTrabajo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int TipoI = Convert.ToInt32(Request.QueryString["TipoI"].ToString());
            int Anio = Convert.ToInt32(Request.QueryString["Anio"].ToString());
            if (!IsPostBack)
            {
                string ReportPath = string.Empty, DsName=string.Empty;
                DataTable dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    DataTable dt3 = new DataTable();

                switch (TipoI)
                {
                    case 1:
                        ReportPath= "Reportes/Rpt/rptVolumenTrabajo.rdlc";
                        SqlParameter sqlParameter = new SqlParameter("@Anio", Anio);
                        dt = getData("sp_rptVolumenTrabajoGlobal", sqlParameter);
                        DsName = "DsVolumenGlobal";
                        break;
                    case 2:
                        ReportPath = "Reportes/Rpt/rpt_VolumenTrabajoResumen.rdlc";
                        SqlParameter sqlParameter2 = new SqlParameter("@Anio", Anio);
                        dt = getData("sp_rptVolumenTrabajoResumen", sqlParameter2);
                        DsName = "DsVolumenResumen";
                        break;
                    case 3:
                        ReportPath = "Reportes/Rpt/rpt_VolumenTrabajoAlcance.rdlc";
                        SqlParameter sqlParameter3 = new SqlParameter("@Anio", Anio);
                        dt = getData("sp_rptVolumenTrabajoAlcance", sqlParameter3);
                        DsName = "DsVolumenAlcance";
                        break;
                    case 4:
                        SqlParameter sqlParameter4 = new SqlParameter("@Anio", Anio);
                        ReportPath = "Reportes/Rpt/rptVolumenTrabajoOtrosDatos.rdlc";
                        dt = getData("sp_rptVolumenTrabajoOtrosDatos1", sqlParameter4);
                        SqlParameter sqlParameter5 = new SqlParameter("@Anio", Anio);
                        dt2 = getData("sp_rptVolumenTrabajoOtrosDatos2", sqlParameter5);
                        SqlParameter sqlParameter6 = new SqlParameter("@Anio", Anio);
                        dt3 = getData("sp_rptVolumenTrabajoResumen", sqlParameter6);
                        break;
                    default:
                        break;
                }

                if (TipoI!=4)
                {
                    ReportViewer1.LocalReport.ReportPath = ReportPath;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.DataSources.Clear();

                    ReportDataSource rds = new ReportDataSource(DsName, dt);
                    ReportViewer1.LocalReport.DataSources.Add(rds);

                    ReportViewer1.LocalReport.DisplayName = DsName.Substring(2, DsName.Length - 2);
                    ReportViewer1.LocalReport.Refresh();
                    ReportViewer1.DataBind();
                }
                else
                {
                    ReportViewer1.LocalReport.ReportPath = ReportPath;
                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    ReportViewer1.LocalReport.DataSources.Clear();

                    ReportDataSource rds = new ReportDataSource("DsVolumenTrabajoOtrosDatos1", dt);
                    ReportViewer1.LocalReport.DataSources.Add(rds);

                    ReportDataSource rds2 = new ReportDataSource("DsVolumenTrabajoOtrosDatos2", dt2);
                    ReportViewer1.LocalReport.DataSources.Add(rds2);

                    ReportDataSource rds3 = new ReportDataSource("DsVolumenResumen", dt3);
                    ReportViewer1.LocalReport.DataSources.Add(rds3);

                    ReportViewer1.LocalReport.DisplayName = "VolumenTrabajoOtrosDatos";
                    ReportViewer1.LocalReport.Refresh();
                    ReportViewer1.DataBind();
                }

            }

        }
        private DataTable getData(string Procedure, SqlParameter sqlParameter=null)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LabmqDB"].ConnectionString);

                if (conn.State==ConnectionState.Closed)
                {
                    conn.Open();
                }
                SqlCommand cmm = new SqlCommand(Procedure, conn);
                cmm.CommandType = CommandType.StoredProcedure;

                if (sqlParameter !=null && !cmm.Parameters.Cast<SqlParameter>().Any(p => p.ParameterName == sqlParameter.ParameterName))
                {
                    cmm.Parameters.Clear();
                    cmm.Parameters.Add(sqlParameter);
                }

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmm;
                dataAdapter.Fill(dt);

                conn.Close();
                conn.Dispose();
                cmm.Dispose();

                return dt;
            }
            catch (Exception ex)
            {

               return dt;
            }



        }
    }
}