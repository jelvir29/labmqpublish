﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmInforme.aspx.cs" Inherits="SistemaLabMQ.Reportes.frmInforme" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="~/img/mq_logo.ico" rel="shortcut icon" />
    <title>LabMQ System</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <rsweb:ReportViewer ID="ReportViewer2" runat="server" Height="579px" Width="1260px">
            </rsweb:ReportViewer>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </div>
    </form>
</body>
</html>
