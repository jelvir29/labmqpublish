﻿/*========================================================================================
Autor:				CARLOS ELVIR
Fecha creación:		04/03/2025
Descripción:		Formatea el valor del resultado en valores correctos que puedan analizarse
******************************************************************************************
							HISTORIAL DE MODIFICACIONES
******************************************************************************************
Nombre:					Descripción:								Fecha:
========================================================================================*/
ALTER FUNCTION dbo.fn_NormalizaResultado (@resultado VARCHAR(50))
RETURNS DECIMAL(18,2)
AS
BEGIN
    DECLARE @valorClean VARCHAR(50) = @resultado;

    -- Si es NULL o cadena vacía, retornar NULL
    IF (@valorClean IS NULL OR LTRIM(RTRIM(@valorClean)) = '')
        RETURN NULL;

	-- Si existe un paréntesis, se extrae la parte antes del mismo
    IF CHARINDEX('(', @valorClean) > 0
    BEGIN
        SET @valorClean = LEFT(@valorClean, CHARINDEX('(', @valorClean) - 1);
    END

    -- Eliminar espacios en blanco al inicio y final
    SET @valorClean = LTRIM(RTRIM(@valorClean));
    -- Eliminar espacios que puedan ser separadores de miles
    SET @valorClean = REPLACE(@valorClean, ' ', '');
    
    -- Reemplazar la coma decimal por un punto
    SET @valorClean = REPLACE(@valorClean, ',', '.');
    
    -- Manejo de notación científica:
    -- Reemplaza patrones como 'x10*' o 'x10e' o 'x10' por 'E'
    SET @valorClean = REPLACE(@valorClean, 'x10*', 'E');
    SET @valorClean = REPLACE(@valorClean, 'x10e', 'E');
    SET @valorClean = REPLACE(@valorClean, 'x10', 'E');
    
    -- Reemplazar superíndices (ejemplo para el ⁴, agregar más si es necesario)
	SET @valorClean = REPLACE(@valorClean, '⁰', '0');
	SET @valorClean = REPLACE(@valorClean, '¹', '1');
	SET @valorClean = REPLACE(@valorClean, '²', '2');
	SET @valorClean = REPLACE(@valorClean, '³', '3');
	SET @valorClean = REPLACE(@valorClean, '⁴', '4');
	SET @valorClean = REPLACE(@valorClean, '⁵', '5');
	SET @valorClean = REPLACE(@valorClean, '⁶', '6');
	SET @valorClean = REPLACE(@valorClean, '⁷', '7');
	SET @valorClean = REPLACE(@valorClean, '⁸', '8');
	SET @valorClean = REPLACE(@valorClean, '⁹', '9');

    -- Si el valor inicia con un comparador, por ejemplo '<'
    IF (LEFT(@valorClean,1) IN ('<','>','=')) 

    -- En este caso, se podría extraer el valor numérico o devolver NULL para no evaluar la validación
    SET @valorClean = SUBSTRING(@valorClean, 2, LEN(@valorClean));

	DECLARE @floatValue FLOAT;
	SET @floatValue = TRY_CAST(@valorClean AS FLOAT);

    RETURN CAST(@floatValue AS DECIMAL(18,2));
END
GO
/*========================================================================================
Autor:				CARLOS ELVIR
Fecha creación:		19/02/2024
Descripción:		Trae la informacion de los analisis de una muesta para el informe
******************************************************************************************
							HISTORIAL DE MODIFICACIONES
******************************************************************************************
Nombre:					Descripción:								Fecha:
CARLOS ELVIR			Inlcuir campo TipoAnalisis					27/11/2024
CARLOS ELVIR			Acreditacion de acuerdo a					04/03/2025 
						validaciones configuradas
========================================================================================*/
ALTER PROCEDURE [dbo].[sp_rptAnalisisSolicitados]
@IdMuestra INT, @NumeroEnmienda INT
AS
BEGIN
IF (@NumeroEnmienda=0)
BEGIN
SELECT 
    A.IdAnalisisSolicitados,
    A.IdCorrelativoMuestra,
    C.TipoAnalisis,
    -- Se elimina cualquier asterisco existente y se concatena según el estado de acreditación
    RTRIM(LTRIM(REPLACE(B.Analisis, '*', ''))) +
      CASE 
          WHEN 
            (
              V.IdValidacion IS NOT NULL
              AND 
              (
                (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
             OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
             OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
             OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
              )
            )
          THEN ' **'
          ELSE 
            CASE B.RequiereTuboEnsayo 
                 WHEN 0 THEN CASE WHEN B.Acreditado = 1 THEN ' *' ELSE ' **' END
                 WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN ' *' ELSE ' **' END
            END
      END AS Analisis,
    -- Ejemplo de validación para análisis cursivos
    CASE 
        WHEN B.Analisis LIKE '%Pseudomonas%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%aureus%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Salmonella%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%coli%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Candida%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Listeria%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%monocytogenes%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Aflatoxinas%' COLLATE Latin1_General_CI_AI THEN 1
        ELSE 0
    END AS EsCursiva,
    A.Resultado,
    A.Unidades,
    A.ValorRecomendado,
    A.ValoresMaximos,
    A.ConcentracionMaxima,
    A.EspecificacionMateriaPrima,
    A.EspecificacionProductoTerminado,
    A.Limites,
    A.RangoAceptacion,
    A.Metodo,
    A.FechaResultado,
    -- Cálculo final de la acreditación considerando las validaciones
    CASE 
         WHEN V.IdValidacion IS NOT NULL 
              AND (
                (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
             OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
             OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
             OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
              )
         THEN 0
         ELSE 
            CASE B.RequiereTuboEnsayo
                 WHEN 0 THEN B.Acreditado
                 WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN 1 ELSE 0 END
            END
    END AS Acreditado
FROM AnalisisSolicitados A
INNER JOIN Analisis B ON A.IdAnalisis = B.IdAnalisis
INNER JOIN TiposAnalisis C ON B.IdTipoAnalisis = C.IdTipoAnalisis
LEFT JOIN ValidacionesAnalisis V ON B.IdAnalisis = V.IdAnalisis
WHERE A.IdCorrelativoMuestra = @IdMuestra
ORDER BY A.IdAnalisisSolicitados ASC;

END
ELSE
BEGIN 
SELECT 
    A.IdAnalisisSolicitados,
    A.IdCorrelativoMuestra,
    C.TipoAnalisis,
    -- Se elimina cualquier asterisco existente y se concatena según el estado de acreditación
    RTRIM(LTRIM(REPLACE(B.Analisis, '*', ''))) +
      CASE 
          WHEN 
            (
              V.IdValidacion IS NOT NULL
              AND 
              (
                (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
             OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
             OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
             OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
              )
            )
          THEN ' **'
          ELSE 
            CASE B.RequiereTuboEnsayo 
                 WHEN 0 THEN CASE WHEN B.Acreditado = 1 THEN ' *' ELSE ' **' END
                 WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN ' *' ELSE ' **' END
            END
      END AS Analisis,
    -- Ejemplo de validación para análisis cursivos
    CASE 
        WHEN B.Analisis LIKE '%Pseudomonas%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%aureus%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Salmonella%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%coli%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Candida%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Listeria%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%monocytogenes%' COLLATE Latin1_General_CI_AI
          OR B.Analisis LIKE '%Aflatoxinas%' COLLATE Latin1_General_CI_AI THEN 1
        ELSE 0
    END AS EsCursiva,
    A.Resultado,
    A.Unidades,
    A.ValorRecomendado,
    A.ValoresMaximos,
    A.ConcentracionMaxima,
    A.EspecificacionMateriaPrima,
    A.EspecificacionProductoTerminado,
    A.Limites,
    A.RangoAceptacion,
    A.Metodo,
    A.FechaResultado,
    -- Cálculo final de la acreditación considerando las validaciones
    CASE 
         WHEN V.IdValidacion IS NOT NULL 
              AND (
                (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
             OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
             OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
             OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
              )
         THEN 0
         ELSE 
            CASE B.RequiereTuboEnsayo
                 WHEN 0 THEN B.Acreditado
                 WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN 1 ELSE 0 END
            END
    END AS Acreditado
FROM AnalisisSolicitadosEnmienda A
INNER JOIN Analisis B ON A.IdAnalisis = B.IdAnalisis
INNER JOIN TiposAnalisis C ON B.IdTipoAnalisis = C.IdTipoAnalisis
LEFT JOIN ValidacionesAnalisis V ON B.IdAnalisis = V.IdAnalisis
WHERE A.IdCorrelativoMuestra = @IdMuestra
AND A.NumeroEnmienda=@NumeroEnmienda
ORDER BY A.IdAnalisisSolicitados ASC;
END
END
GO
/*========================================================================================
Autor:				CARLOS ELVIR
Fecha creación:		03/02/2024
Descripción:		Trae la incertidumbre de los analisis de una muestra
******************************************************************************************
							HISTORIAL DE MODIFICACIONES
******************************************************************************************
Nombre:					Descripción:								Fecha:
CARLOS ELVIR			SE AGREGÓ LA INCETIDUMBRE COMO RESULTADO	09/12/2024
CARLOS ELVIR			Acreditacion de acuerdo a					04/03/2025 
						validaciones configuradas
========================================================================================*/
ALTER PROCEDURE [dbo].[sp_rptEnsayoNoAcreditado]
	@IdCorrelativoMuestra INT, @NumeroEnmienda INT, @IdAnalisisSolicitado INT=0
AS
BEGIN
	/*SELECT  [IdRow],[Ensayo],[Incertidumbre]
    FROM [EnsayoNoAcreditado]
    where IdCorrelativoMuestra=@IdCorrelativoMuestra*/
IF(@IdAnalisisSolicitado = 0)
BEGIN
    IF(@NumeroEnmienda = 0)
    BEGIN
        SELECT 
            A.IdAnalisisSolicitados AS IdRow,
            RTRIM(LTRIM(REPLACE(B.Analisis, '*', ''))) +
              CASE 
                  WHEN 
                    (
                      V.IdValidacion IS NOT NULL AND 
                      (
                        (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
                     OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
                     OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
                     OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
                      )
                    )
                  THEN ' **'
                  ELSE 
                    CASE B.RequiereTuboEnsayo 
                         WHEN 0 THEN CASE WHEN B.Acreditado = 1 THEN ' *' ELSE ' **' END
                         WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN ' *' ELSE ' **' END
                    END
              END AS Ensayo,
            '± ' + LTRIM(RTRIM(A.Incertidumbre)) + ' ' + B.Unidades AS Incertidumbre
        FROM AnalisisSolicitados A
        INNER JOIN Analisis B 
            ON A.IdAnalisis = B.IdAnalisis
        LEFT JOIN ValidacionesAnalisis V 
            ON B.IdAnalisis = V.IdAnalisis
        WHERE A.IdCorrelativoMuestra = @IdCorrelativoMuestra 
          AND LEN(LTRIM(RTRIM(A.Incertidumbre))) > 0;
    END
    ELSE
    BEGIN
        SELECT 
            A.IdAnalisisSolicitados AS IdRow,
            RTRIM(LTRIM(REPLACE(B.Analisis, '*', ''))) +
              CASE 
                  WHEN 
                    (
                      V.IdValidacion IS NOT NULL AND 
                      (
                        (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
                     OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
                     OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
                     OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
                      )
                    )
                  THEN ' **'
                  ELSE 
                    CASE B.RequiereTuboEnsayo 
                         WHEN 0 THEN CASE WHEN B.Acreditado = 1 THEN ' *' ELSE ' **' END
                         WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN ' *' ELSE ' **' END
                    END
              END AS Ensayo,
            '± ' + LTRIM(RTRIM(A.Incertidumbre)) + ' ' + B.Unidades AS Incertidumbre
        FROM AnalisisSolicitadosEnmienda A
        INNER JOIN Analisis B 
            ON A.IdAnalisis = B.IdAnalisis
        LEFT JOIN ValidacionesAnalisis V 
            ON B.IdAnalisis = V.IdAnalisis
        WHERE A.IdCorrelativoMuestra = @IdCorrelativoMuestra 
          AND LEN(LTRIM(RTRIM(A.Incertidumbre))) > 0 
          AND A.NumeroEnmienda = @NumeroEnmienda;
    END
END
ELSE
BEGIN
    IF(@NumeroEnmienda = 0)
    BEGIN
        SELECT 
            A.IdAnalisisSolicitados AS IdRow,
            RTRIM(LTRIM(REPLACE(B.Analisis, '*', ''))) +
              CASE 
                  WHEN 
                    (
                      V.IdValidacion IS NOT NULL AND 
                      (
                        (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
                     OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
                     OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
                     OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
                      )
                    )
                  THEN ' **'
                  ELSE 
                    CASE B.RequiereTuboEnsayo 
                         WHEN 0 THEN CASE WHEN B.Acreditado = 1 THEN ' *' ELSE ' **' END
                         WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN ' *' ELSE ' **' END
                    END
              END AS Ensayo,
            '± ' + LTRIM(RTRIM(A.Incertidumbre)) + ' ' + B.Unidades AS Incertidumbre
        FROM AnalisisSolicitados A
        INNER JOIN Analisis B 
            ON A.IdAnalisis = B.IdAnalisis
        LEFT JOIN ValidacionesAnalisis V 
            ON B.IdAnalisis = V.IdAnalisis
        WHERE A.IdCorrelativoMuestra = @IdCorrelativoMuestra 
          AND A.IdAnalisisSolicitados = @IdAnalisisSolicitado
          AND LEN(LTRIM(RTRIM(A.Incertidumbre))) > 0;
    END
    ELSE
    BEGIN
        SELECT 
            A.IdAnalisisSolicitados AS IdRow,
            RTRIM(LTRIM(REPLACE(B.Analisis, '*', ''))) +
              CASE 
                  WHEN 
                    (
                      V.IdValidacion IS NOT NULL AND 
                      (
                        (V.Operador = 'ENTRE' AND dbo.fn_NormalizaResultado(A.Resultado) BETWEEN V.Valor1 AND V.Valor2)
                     OR (V.Operador = '>'     AND dbo.fn_NormalizaResultado(A.Resultado) > V.Valor1)
                     OR (V.Operador = '<'     AND dbo.fn_NormalizaResultado(A.Resultado) < V.Valor1)
                     OR (V.Operador = '='     AND (B.Metodo = V.Metodo OR dbo.fn_NormalizaResultado(A.Resultado) = V.Valor1))
                      )
                    )
                  THEN ' **'
                  ELSE 
                    CASE B.RequiereTuboEnsayo 
                         WHEN 0 THEN CASE WHEN B.Acreditado = 1 THEN ' *' ELSE ' **' END
                         WHEN 1 THEN CASE WHEN A.TipoTubo = 5 THEN ' *' ELSE ' **' END
                    END
              END AS Ensayo,
            '± ' + LTRIM(RTRIM(A.Incertidumbre)) + ' ' + B.Unidades AS Incertidumbre
        FROM AnalisisSolicitadosEnmienda A
        INNER JOIN Analisis B 
            ON A.IdAnalisis = B.IdAnalisis
        LEFT JOIN ValidacionesAnalisis V 
            ON B.IdAnalisis = V.IdAnalisis
        WHERE A.IdCorrelativoMuestra = @IdCorrelativoMuestra 
          AND A.IdAnalisisSolicitados = @IdAnalisisSolicitado
          AND LEN(LTRIM(RTRIM(A.Incertidumbre))) > 0 
          AND A.NumeroEnmienda = @NumeroEnmienda;
    END
END

END
GO
/*========================================================================================
Autor:				CARLOS ELVIR
Fecha creación:		05/03/2025
Descripción:		Elimina una entrada y sus muestras
******************************************************************************************
							HISTORIAL DE MODIFICACIONES
******************************************************************************************
Nombre:					Descripción:								Fecha:
========================================================================================*/
CREATE PROCEDURE [dbo].[sp_DeleteEntrada](@IdEntrada INT, @NumeroEnmienda INT)
AS
BEGIN

--VALIDAR QUE NO EXISTAN ENTRADAS DESPUES DE LA QUE SE QUIERE BORRAR

IF EXISTS(SELECT 1 FROM Entradas WHERE IdCorrelativoEntrada=@IdEntrada+1 AND NumeroEnmienda=@NumeroEnmienda)
BEGIN
	SELECT 'Ya existe una entrada despues de la entrada que se quiere eliminar' AS Error
END
ELSE
BEGIN
DELETE FROM Entradas WHERE IdCorrelativoEntrada=@IdEntrada AND NumeroEnmienda=@NumeroEnmienda;

DELETE FROM  SeccionGeneral WHERE IdCorrelativoEntrada=@IdEntrada AND NumeroEnmienda=@NumeroEnmienda;

DELETE FROM CondicionesMuestra WHERE IdCorrelativoEntrada=@IdEntrada AND NumeroEnmienda=@NumeroEnmienda;

DELETE FROM UsoInterno WHERE IdCorrelativoEntrada=@IdEntrada AND NumeroEnmienda=@NumeroEnmienda;

DELETE FROM paramInformeResultados WHERE IdMuestra IN(SELECT IdMuestra FROM Muestras WHERE IdCorrelativoEntrada=@IdEntrada AND NumeroEnmienda=@NumeroEnmienda) 
AND NumeroEnmienda=@NumeroEnmienda;

DELETE FROM Muestras WHERE IdCorrelativoEntrada=@IdEntrada AND NumeroEnmienda=@NumeroEnmienda;

DELETE FROM AnulacionEntradas WHERE IdCorrelativoEntrada=@IdEntrada AND NumeroEnmienda=@NumeroEnmienda;

SELECT 'Se ha eliminado la entrada No. '+CONVERT(VARCHAR(20), @IdEntrada) AS Error
END
END
GO
SELECT Resultado, dbo.fn_NormalizaResultado(Resultado), IdCorrelativoMuestra, A.IdAnalisis,c.Analisis, 
B.MensajeNoAcreditado, Operador, b.Metodo, a.Metodo, Valor1, CASE WHEN A.Metodo = B.Metodo THEN 1 ELSE 0 END
FROM AnalisisSolicitados A inner join ValidacionesAnalisis B ON A.IdAnalisis=B.IdAnalisis
inner join Analisis c on a.IdAnalisis=c.IdAnalisis
WHERE IdCorrelativoMuestra=114855
order by 3 desc
