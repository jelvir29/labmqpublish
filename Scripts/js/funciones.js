﻿(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

function AnularMuestra(idMuestra) {
    document.getElementById('idMuestraAnulacion').value = idMuestra;
}

function buscarCliente(idCliente) {
    var url = "../Clientes/BuscarCliente?idCliente=" + idCliente;
    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        success: function (data) {
            if (data != null) {
                document.getElementById('IdCliente').value = idCliente;
                document.getElementById('Empresa').value = data.Cliente;
                document.getElementById('Contacto').value = data.Contacto;
                document.getElementById('Telefono').value = data.Telefono;
                document.getElementById('Direccion').value = data.Direccion;
            }
        },
        processData: false,
        contentType: false
    })
}

$(document).ready(function () {
    $('#dataTable3').dataTable();
    $('#dataTable2').dataTable();
    $('#dataTable').dataTable();
    $('#dataTableEntradasTecnicos').dataTable({
        "order": [[0, "desc"]]
    });
    $('#dataTableMuestrasTecnicos').dataTable({
        "order": [[0, "desc"]]
    });
});
